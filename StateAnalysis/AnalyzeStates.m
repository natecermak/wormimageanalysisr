%%%%%%%%%%%%%%%%%%%%%%%     Set Directory     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all

main_directory      = '';
loading_directory   = strcat(main_directory, '\','Load');  
saving_directory    = strcat(main_directory, '\','Data'); 

%Load Data 
DataSet_Struct = {''};

addpath(genpath(pwd))

for d = 1:length(DataSet_Struct)
    
%%%%%%%%%%%%%%%%%%%%%%%     Extract Data     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Extract Worm Data 
load_animal_file      = DataSet_Struct{d}; 
disp(load_animal_file)
load_animal_file_path = strcat(loading_directory,'\',load_animal_file); 
save_animal_directory      = strcat(saving_directory, '\',load_animal_file);
mkdir(save_animal_directory)
%Extract Data
[Velocity, AngVelocity, Pump, Posture, ... 
    DMP, Egg, LaserTimes,AutoFocus, FramesVector,OffFoodHead, OffFoodBody, num_animals]...
    = ExtractData(load_animal_file_path, 1); 

this_var = strcat(save_animal_directory, '\', 'Posture_', load_animal_file); 
save(this_var, 'Posture', '-v7.3'); 

this_var = strcat(save_animal_directory, '\', 'RawBehaviors_',load_animal_file); 
save(this_var, 'Velocity', 'AngVelocity','Pump','DMP','Egg', 'LaserTimes', 'AutoFocus', 'OffFoodHead', 'OffFoodBody','-v7.3'); 
 

%%%%%%%%%%%%%%%%%%%%%%%     Format Postures     %%%%%%%%%%%%%%%%%%%%%%%%%%%

%Load Posture 
load_animal_file_path      = strcat(save_animal_directory,'\','Posture_', load_animal_file); 
load(load_animal_file_path);

%Load compendium 
ref_posture_file    = 'compendium100VD.csv'; 
load_ref_posture_file = strcat(loading_directory,'\',ref_posture_file); 

%Format Posture 
num_posture_matrix_cols          = 19; 
temp_posture_matrix              = Posture(:, 1:15); 
num_frames                       = length(temp_posture_matrix); 
Posture_Matrix                   = nan(num_frames, num_posture_matrix_cols);

%Set All NaN Rows
NaNRows                          = find(isnan(temp_posture_matrix(:,2)));
for i = 1: length(NaNRows)
    this_id = NaNRows(i); 
    temp_posture_matrix(this_id,2:15)     = nan(1,14);
end    

Posture_Matrix(:,1:15)        = temp_posture_matrix;  

%Match To Compendium 
num_segments = 14; 
[Posture_Matrix_Compendium, Posture_Matrix]= CompendiumMatch(num_segments,...
    load_ref_posture_file, Posture_Matrix, num_frames); 

temp_posture_matrix = Posture_Matrix_Compendium;

%Make Posture Struct Separated By Animal 
animal_number_vec          =  Posture_Matrix(:, 1);

all_postures               =  Posture_Matrix(:, 2:num_posture_matrix_cols); 
all_compendium_postures    = Posture_Matrix_Compendium(:,2:15); 


%Make All Posture / Compendium 
Posture_Main_Struct         = {};   

for a = 1:num_animals
    disp(a)
    animal_idx                      = find(animal_number_vec == a); 
    
    %All
    this_posture                    = all_postures(animal_idx, :); 
    Posture_Main_Struct{a}          =  this_posture; 
    
    %Compendium 
    this_posture                    = all_compendium_postures(animal_idx, :);
    Posture_Compendium_Struct{a}    = this_posture;  
end

%Save Posture_Main_Struct
this_var1    = strcat(save_animal_directory, '\', 'Posture_Main_Struct_', load_animal_file); 
save(this_var1, 'Posture_Main_Struct', '-v7.3')

%
%%%%%%%%%%%%%%%%%%     Add Transition Cluster to Data    %%%%%%%%%%%%%%%%%%
load_file = strcat(loading_directory,'\','model_centroids.mat'); 
load(load_file)

load_file = strcat(loading_directory,'\','C.mat'); 
load(load_file)

best_k_means         = C{best_k, model_idx}; 

nan_tc     = best_k + 1; 
num_comp_post = 100; 
for a  = 1:num_animals
    this_Posture = Posture_Main_Struct{a};
    
    for p = 1:num_comp_post
        idx = find(this_Posture(:,15)==p);
        this_Posture(idx,16) = best_k_means(p);  
    end
    
    % Add nan to last transition cluster 
    nan_idx = find(isnan(this_Posture(:,1))); 
    this_Posture(nan_idx, 16) = ones(length(nan_idx), 1)* nan_tc; 
    Posture_Main_Struct{a} = this_Posture; 
end

%Save data 
posture_main_struct_path    = strcat(save_animal_directory, '\', 'Posture_Main_Struct_', load_animal_file); 
save(posture_main_struct_path, 'Posture_Main_Struct', '-v7.3')

%
%%%%%%%%%%%%%%%%%%     Relabel by Optimal Clustering    %%%%%%%%%%%%%%%%%%%
fps         = 20; 
bin_sec     = 3; 
num_t_clust = nan_tc; 


%Bin Data
[bin_percent_struct_path, data_key_percent_struct_path, ...
    num_postures_per_bin_path, num_bins_per_animal_path] = ...
    BinFrames(posture_main_struct_path, ...
    fps, bin_sec, num_t_clust, save_animal_directory); 

%Load data key 
load(data_key_percent_struct_path)

%Load bin percentage structure 
load(bin_percent_struct_path)

%Load bin num postures structure 
load(num_postures_per_bin_path)



%Kmeans 
k_to_use = 8;


%Load centers
load_file = strcat(loading_directory,'\','trans_cluster_centers.mat'); 
load(load_file)



%Make clustering data 
%Make eq_sampled clustering data 
trans_clustering_input = []; 


for a = 1: num_animals
     this_data = bin_percent_struct{a}; 
     trans_clustering_input = [trans_clustering_input; this_data];    
end


bin_sec = 3; 
fps     = 20; 
bin_size = bin_sec*fps; 

HMM_input = {}; 

%Relabel 
for a = 1: num_animals
    
    this_animal_data = bin_percent_struct{a}; 
    this_Posture_Main = Posture_Main_Struct{a}; 
  
    key_bin_col     = data_key{a}(:,2); 
    total_num_bins = max(key_bin_col);     
    data_centers    = trans_cluster_centers; 
    clustering_idx = knnsearch(data_centers, this_animal_data);
    
    this_HMM_input = nan(1,total_num_bins);

        
    for b = 1:total_num_bins
        
        this_bin_idx    = find(key_bin_col == b);
        bin_len         = length(this_bin_idx); 
        this_bins_cluster   = clustering_idx(b);
        this_HMM_input(1,b) = this_bins_cluster; 
        this_Posture_Main(this_bin_idx, 17) = this_bins_cluster*ones(bin_len,1); 
 
    end    
 
    Posture_Main_Struct{a}  = this_Posture_Main;    
    HMM_input{a}            = this_HMM_input; 

end

%Save 

Posture_Main_Struct_path  = strcat(save_animal_directory, '\', 'Posture_Main_Struct_', load_animal_file); 
save(Posture_Main_Struct_path, 'Posture_Main_Struct', '-v7.3')

Posture_Main_Struct_path  = strcat(save_animal_directory, '\', 'HMM_input_', load_animal_file); 
save(Posture_Main_Struct_path, 'HMM_input', '-v7.3')


%
%%%%%%%%%%%%%%%% Add HMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load_file = strcat(loading_directory,'\','this_EMTM.mat'); 
load(load_file) 

for a = 1:num_animals
    disp(a)
    this_seq = HMM_input{1,a}; 
    [states] = hmmviterbi(this_seq, this_TM, this_EM); 
    gen_states{a} = states; 
end

bin_sec = 3; 
fps     = 20; 
bin_size = bin_sec*fps; 


%Relabel 
for a = 1: num_animals
    
    this_animal_data = gen_states{a}; 
    this_Posture_Main = Posture_Main_Struct{a}; 
  
    key_bin_col     = data_key{a}(:,2); 
    total_num_bins = max(key_bin_col);     
    

        
    for b = 1:total_num_bins
        
        this_bin_idx    = find(key_bin_col == b);
        bin_len         = length(this_bin_idx); 
        this_bins_state   = this_animal_data(b);
        this_Posture_Main(this_bin_idx, 18) = this_bins_state*ones(bin_len,1); 
 
    end    
 
    Posture_Main_Struct{a}  = this_Posture_Main;    

end



%Save 
Posture_Main_Struct_path  = strcat(save_animal_directory, '\', 'Posture_Main_Struct_', load_animal_file); 
save(Posture_Main_Struct_path, 'Posture_Main_Struct', '-v7.3')

end
