function[Posture_Matrix_Compendium, Posture_Matrix]= CompendiumMatch(num_segments,...
    load_ref_posture_file, Posture_Matrix, num_frames)
% Match postures with corresponding compendium 

% Find cluster centers from reference file 
cluster_avg = FileExtract(load_ref_posture_file, ...
    num_segments); 

% Classify postures by compendium by KNN
all_postures = Posture_Matrix(:,2:15);
IDX = knnsearch(cluster_avg,all_postures);
%For each posture, find nearest neighbor
    %if none, nan 

posture_temp         = nan(num_frames, 16);
posture_temp(:,16)   = IDX;

nan_rows                     = find(isnan(Posture_Matrix(:,2)));
posture_temp(nan_rows,16)    = NaN;

%Add Compendium Posture Number to Posture Main
Posture_Matrix(:,16)        = posture_temp(:,16);  


% Relabel All Postures By Compendium 
num_comp_post = 100; 
Posture_Matrix_Compendium = nan(num_frames, 15);

Posture_Matrix_Compendium(:,1) = Posture_Matrix(:,1); 

for c = 1: num_comp_post
    
    these_post_idx = find(posture_temp(:,16) == c); 
    this_comp_angles = cluster_avg(c,:);
    
    for i = 1: length(these_post_idx)
    
        this_ind_here = these_post_idx(i); 
        Posture_Matrix_Compendium(this_ind_here, 2:15) = this_comp_angles; 
    end

end
end

