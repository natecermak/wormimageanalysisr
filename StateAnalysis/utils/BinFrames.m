function [bin_percent_struct_path, data_key_percent_struct_path, ...
    num_postures_per_bin_path, num_bins_per_animal_path] = ...
    BinFrames(posture_main_struct_path, ...
    fps, bin_sec, num_clust, Bin_directory)
%Bin Frames and return percentage occurence matrix 

%Load Posture
load(posture_main_struct_path); 

%Separate into bins 
t_bin           = fps*bin_sec;
num_animals     = length(Posture_Main_Struct);


bin_percent_struct              = {}; 
num_postures_per_bin             = {}; 
data_key                        = {};
num_bins_per_animal             = nan(24,1); 


for a = 1:num_animals
    Posture           = Posture_Main_Struct{a}; 
    this_comp_data    = Posture(:,16); 
    
    this_num_frames = size(this_comp_data);
    this_num_frames = this_num_frames(1); 
    
    num_bins        = this_num_frames - rem(this_num_frames, t_bin); 
    num_bins        = num_bins / t_bin; 
    
    num_bins_per_animal(a,1) = num_bins; 
        
    this_animal_binned_per  = nan(num_bins, num_clust); 
    
    start_ind = 1; 
    
    this_data_key       = nan(this_num_frames, 3);
    this_data_key(:,1)  = [1:1:this_num_frames];
    
    this_num_postures_per_bin            = nan(num_bins,1); 

 
    for b = 1:num_bins
        end_ind                             = b*t_bin; 
        this_bin_data                       = this_comp_data(start_ind:end_ind, 1);
        this_data_key(start_ind:end_ind,2)  = (ones(length(this_bin_data),1))*b;      
            
        this_bin_per                        = zeros(1,num_clust); 
        
        for c = 1:num_clust 
            num_c = length(find(this_bin_data == c)); 
            this_bin_per(1,c) = num_c; 
        end
        bin_per_sum  = sum(this_bin_per); 
        this_bin_per = this_bin_per ./ bin_per_sum; 
        this_animal_binned_per(b,:) = this_bin_per; 
        
        bin_is_nan = isnan(this_bin_per); 
        
        if sum(bin_is_nan) == 0
            
            this_data_key(start_ind:end_ind,3)  = ones(length(this_bin_data),1); 
            num_p_in_bin                        = sum(this_bin_per ~= 0); 
            this_num_postures_per_bin(b,1)       = num_p_in_bin; 
        
        elseif sum(bin_is_nan) == 1
           this_data_key(start_ind:end_ind,3)  = zeros(length(this_bin_data),1);      

        end    

        start_ind    = start_ind + t_bin; 
    end 
    
     bin_percent_struct{a}      = this_animal_binned_per; 
     data_key{a}                = this_data_key;
     num_postures_per_bin{a}    = this_num_postures_per_bin; 

end

%Save data 
bin_percent_struct_path      = strcat(Bin_directory, '\', 'bin_percent_struct'); 
save(bin_percent_struct_path, 'bin_percent_struct'); 


data_key_percent_struct_path = strcat(Bin_directory, '\', 'data_key'); 
save(data_key_percent_struct_path, 'data_key');


num_postures_per_bin_path = strcat(Bin_directory, '\', 'num_postures_per_bin'); 
save(num_postures_per_bin_path, 'num_postures_per_bin');


num_bins_per_animal_path = strcat(Bin_directory, '\', 'num_bins_per_animal'); 
save(num_bins_per_animal_path, 'num_bins_per_animal');


end

