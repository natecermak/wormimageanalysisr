function [Speed, AngSpeed, Pump, Posture, ... 
    DMPsFinal, EggsFinal, LaserTimes,AutoFocus,FramesVector, OffFoodHead, OffFoodBody, num_animals]...
    = ExtractData(data_folder_path, stIn)

%Get Data into Useable Format 
%Input: name of folder in the form of a string path (i.e. 'C: \Users\ .. Data')
%Output: Speed, AngSpeed, Pump, Posture, DMPsFinal, EggsFinal, LaserTimes, FramesVector

folderList            = dir(data_folder_path);            %create list of folders in directory 
num_files             = length(folderList); 
num_animals           = num_files - 2; 


FramesVector        = [];       %frame vector 


for i = 3:num_files
        

        file_name = folderList(i).name;
        fmt = repmat('%f',1,50);
        full_file_path = strcat(data_folder_path, '\', file_name);
        fid = fopen(full_file_path, 'rt');
        indata = textscan(fid, fmt, 'Headerlines', 1, 'Delimiter', ',', 'TreatAsEmpty', 'NA', 'CollectOutput', 1);
        Temp = indata{1};
        clear('indata');

%If you need to trim recording        
%         i2  = i -2; 
%         if i2 == 36
%             end_frame = 10.5*10^4; 
%             Temp      = Temp(1:end_frame,:); 
%         end

        numDat = length(Temp(:,1));

        %Speed 
        SpeedCol = 44; 
        Speed(stIn:(stIn+numDat-1),1) = i; 
        Speed(stIn:(stIn+numDat-1),2) = Temp(:,SpeedCol);

        %Angular Speed 
        AngSpeedCol = 46; 
        AngSpeed(stIn:(stIn+numDat-1),1) = i; 
        AngSpeed(stIn:(stIn+numDat-1),2) = Temp(:,AngSpeedCol);

        %Pump
        PumpCol = 41; 
        Pump(stIn:(stIn+numDat-1),1) = i; 
        Pump(stIn:(stIn+numDat-1),2) = Temp(:,PumpCol);
        
        %Food 
        FoodHeadcol = 35; 
        rawFoodHeadDist = Temp(:,FoodHeadcol); 
        BinaryFoodHead = zeros(1,numDat); 
        BinaryFoodHead(find(rawFoodHeadDist>0)) = 1;
        OffFoodHead(stIn:(stIn+numDat-1),1) = i; 
        OffFoodHead(stIn:(stIn+numDat-1),2) = BinaryFoodHead;
        
        FoodBodycol = 36; 
        rawFoodBodyDist = Temp(:,FoodBodycol); 
        BinaryFoodBody = zeros(1,numDat); 
        BinaryFoodBody(find(rawFoodBodyDist>0)) = 1;
        OffFoodBody(stIn:(stIn+numDat-1),1) = i; 
        OffFoodBody(stIn:(stIn+numDat-1),2) = BinaryFoodBody; 
        
        % Eggs 
        EggLocCol = 39; 
        EggLocMean(i) = Temp(1,EggLocCol);

        %Posture
        PostureCol = 4:17; 
        PostureTemp = Temp(:,PostureCol);

        %Laser 
        laserCol = 49; 
        LaserTimes(stIn:(stIn+numDat-1),1) = i; 
        LaserTimes(stIn:(stIn+numDat-1),2) = Temp(:,laserCol);
        
        %AutoFocus
        autofocusCol = 48;
        AutoFocus(stIn:(stIn+numDat-1),1) = i; 
        AutoFocus(stIn:(stIn+numDat-1),2) = Temp(:,autofocusCol);
        
        

        %%%%%%%%%%%%%%%%%%%%%%    Process the Posture   %%%%%%%%%%%%%%%%%%%%%%%

        if(EggLocMean(i)>=7)
            PostureTemp = PostureTemp*-1;
            disp_str    = strcat("flipped_", num2str(i-2));
            disp(disp_str)
        end

        PostureTemp2 = bsxfun(@minus,PostureTemp,nanmean(PostureTemp,2));

        Posture(stIn:(stIn+numDat-1),1) = i-2; 
        Posture(stIn:(stIn+numDat-1),2:15) = PostureTemp2;
        clear('PostureTemp')
        clear('PostureTemp2')


        %DMP 
        DMPcol = 37; 
        DMPsFinal(stIn:(stIn+numDat-1),1) = i; 
        DMPsFinal(stIn:(stIn+numDat-1),2) = Temp(:,DMPcol);

        %Eggs 
        Eggcol = 38; 
        EggsFinal(stIn:(stIn+numDat-1),1) = i; 
        EggsFinal(stIn:(stIn+numDat-1),2) = Temp(:,Eggcol);


        %Frames
        FramesHere = 1:1:numDat;
        FramesVector = [FramesVector FramesHere];


        stIn = stIn+numDat;
        clear('Temp')
        fclose(fid);

end
end


