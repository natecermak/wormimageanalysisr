function [Extracted] = FileExtract(filename, num_cols)
%Extract information from excel file
    fmt = repmat('%f',1,num_cols);
    fid = fopen(filename, 'rt');
    indata = textscan(fid, fmt, 'Headerlines', 1, 'Delimiter', ...
    ',', 'TreatAsEmpty', 'NA', 'CollectOutput', 1);
    Extracted = indata{1};
end

