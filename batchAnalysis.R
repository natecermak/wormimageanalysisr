dirs = list.files("G:/", include.dirs=T, pattern="2017",full.names=T)
ll <- parse(file = "C:/Users/labmember/Dropbox (MIT)/Flavell lab/wormImageAnalysisR/analyzeWorms.R")


for (d in dirs){
  imageDir = paste(d,"/images/",sep='')
  setwd(imageDir)
  csvFile = list.files(d, pattern='tracking', full.names=T)
  jpegDirectory = imageDir
  lawnBoundaryFile = list.files(d, pattern='lawn', full.names=T)
  
  ## run script
  for (i in seq_along(ll)) {
    tryCatch(eval(ll[[i]]), 
             error = function(e) message(as.character(e)))
  }
  cat(paste(c("--------- !!!!!!!!!  COMPLETED ", d, " !!!!!!!!!!!!! ------------ \n"),sep=''))

  rm(list=setdiff(ls(), c("d","dirs","ll")))
}