#include <Rcpp.h>
using namespace Rcpp;

bool inImageRange(NumericMatrix im, int y, int x){
  return (y >= 0 && y < im.nrow() && x >= 0 && x < im.ncol());
}

double dnorm(double x, double mu, double sigma){
  return( exp( -pow(x-mu,2)/(2*sigma*sigma)));
}

// [[Rcpp::export]]
NumericMatrix createWormPostureImage(NumericMatrix pred, NumericVector params,
                                      NumericMatrix bbBasis, NumericMatrix widthBasis) {

  double dL = params[0];
  double y = params[1];
  double x = params[2];

  int edge1x, edge1y, edge2x, edge2y;
  int i,j;
  double perpx, perpy; //vector perpendicular to backbone

  // initialize matrix pred to all zeros
  for (i=0; i<pred.nrow(); i++)
    for (j=0; j<pred.ncol(); j++)
      pred(i,j)=0;

  // calculate all 1000 backbone angles from spline (just a matrix multiply)
  NumericVector angles(bbBasis.nrow());
  for(i=0; i<bbBasis.nrow(); i++){
    angles[i] = 0;
    for (j=0; j < bbBasis.ncol(); j++)
      angles[i] += params[3+j] * bbBasis(i,j);
  }
  // calculate all 1000 body widths from spline (again, just a matrix multiply)
  NumericVector width(bbBasis.nrow());
  for(i=0; i<bbBasis.nrow(); i++){
    width[i] = 0;
    for (j=0; j < widthBasis.ncol(); j++)
      width[i] += params[3+j+bbBasis.ncol()] * widthBasis(i,j);
  }


  for (i=0; i< bbBasis.nrow(); i++){ // for each point along the backbone
    perpx = cos(angles[i]+1.57079632679); //calculate the normal vector
    perpy = sin(angles[i]+1.57079632679);
    for (double w=0; w<width[i]/2; w=w+0.5){ //iterate along the normal vector, filling in pixels as we go.
      edge1x = round(x + w*perpx);
      edge1y = round(y + w*perpy);
      edge2x = round(x - w*perpx);
      edge2y = round(y - w*perpy);

      if (inImageRange(pred, edge1y, edge1x))
        pred(edge1y, edge1x) = 1;
      if (inImageRange(pred, edge2y, edge2x))
        pred(edge2y, edge2x) = 1;
    }

    x += dL * cos(angles[i]);
    y += dL * sin(angles[i]);
  }
  return pred;
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically
// run after the compilation.
//

/*** R
library(splines)
library(EBImage)
library(caTools)
bbBasis = bs(seq(0,1,length=1000), knots=0:10/10, degree=2)
bbBasis = bbBasis[,-ncol(bbBasis)]
widthBasis = bs(seq(0,1,length=1000), knots=c(0, 0.1, 0.2, 0.8, 0.9, 1), degree=2)
widthBasis = widthBasis[,-c(1,ncol(widthBasis)-0:1)]

bbSplineWeights = c(0:5/3,5:0/3)
widthSplineWeights = c(10,35,40,35,10)
params = c(dL=0.5, r=300, c=300, bbSplineWeights, widthSplineWeights)
im = matrix(0,ncol=50,nrow=50)
par(mfrow=c(4,4), mar=c(1,1,1,1))
for (j in 1:16){
  bbSplineWeights = cumsum(rnorm(12,0,1))
  bbSplineWeights = bbSplineWeights - mean(bbSplineWeights)
  params = c(dL=0.05, r=25, c=25, bbSplineWeights, 0.1*widthSplineWeights)
  image(createWormPostureImage(im,params,bbBasis,widthBasis), col=gray(0:100/100), axes=F)
}
*/


