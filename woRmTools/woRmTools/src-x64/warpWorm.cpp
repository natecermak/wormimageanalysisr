#include <Rcpp.h>
using namespace Rcpp;


// [[Rcpp::export]]
NumericMatrix warpWorm(NumericMatrix im,
                       NumericVector dL,
                       NumericVector i0,
                       NumericVector angles,
                       NumericVector width)
{

  double ybb = i0[0]; // backbone y coordinate
  double xbb = i0[1]; // backbone x coordinate
  double y,x;
  int i,j;
  double perpy, perpx; //vector perpendicular to backbone

  NumericMatrix warped(angles.size()+1, width[0]);

  for (i=0; i <= angles.size(); i++){ // for each point along the backbone
    perpy = sin(angles[i]+1.57079632679); //calculate the normal vector
    perpx = cos(angles[i]+1.57079632679);
    for (j=0; j<width[0]; j++){ //iterate along the normal vector, filling in pixels as we go.
      y = ybb + perpy*(j - width[0]/2);
      x = xbb + perpx*(j - width[0]/2);
      if (y >= 0 && y<im.nrow() && x>= 0 && x < im.ncol()){
        warped(i,j) = im( (int) round(y), (int) round(x));
      }
    }
    if (i < angles.size()){
      ybb += dL[0] * sin(angles[i]);
      xbb += dL[0] * cos(angles[i]);
    }
  }
  return warped;
}


// [[Rcpp::export]]
NumericMatrix getWormWidths(NumericMatrix im, NumericVector dL, NumericVector i0, NumericVector angles){

  double ybb = i0[0]; // backbone r coordinate
  double xbb = i0[1]; // backbone c coordinate
  double x, y;
  int i;
  int posSteps, negSteps;
  double perpx, perpy; //vector perpendicular to backbone

  NumericMatrix width(angles.size()+1, 2);

  for (i=0; i <= angles.size(); i++){ // for each point along the backbone
    perpx = cos(angles[i]+1.57079632679); //calculate the normal vector
    perpy = sin(angles[i]+1.57079632679);
    posSteps=0;
    negSteps=0;
    x=xbb;
    y=ybb;
    while (x >= 0 && x < im.ncol() && y >= 0 && y < im.nrow()){ //iterate along the normal vector, counting pixels as we go.
      if (im((int) y, (int) x)==0)
        break;
      posSteps++;
      x = xbb + perpx*posSteps;
      y = ybb + perpy*posSteps;
    }
    x=xbb;
    y=ybb;
    while (x >= 0 && x < im.ncol() && y >= 0 && y < im.nrow()){ //iterate along the normal vector, counting pixels as we go.
      if (im((int) y, (int) x)==0)
        break;
      negSteps++;
      x = xbb - perpx*negSteps;
      y = ybb - perpy*negSteps;
    }
    width(i,0) = posSteps;
    width(i,1) = negSteps;


    if (i < angles.size()){
      xbb += dL[0] * cos(angles[i]);
      ybb += dL[0] * sin(angles[i]);
    }
  }
  return width;
}


/*** R


*/
