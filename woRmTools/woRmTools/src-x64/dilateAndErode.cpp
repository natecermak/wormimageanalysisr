#include <Rcpp.h>
using namespace Rcpp;


// [[Rcpp::export]]
IntegerMatrix boxDilate(IntegerMatrix im, IntegerVector iters) {
  // This function should handle boundaries just fine. 
  
  for (int iter=0; iter < iters[0]; iter++){
    for (int r=1; r < im.nrow()-1; r++){   // skip the first and last rows
      for (int c=1; c < im.ncol()-1; c++){ // skip the first and last columns;
        if (im(r,c) > 0 && im(r,c) < iter+2){  // if the pixel is bright and we didn't just make it bright this iteration, then
          if (!im(r-1,c-1)) im(r-1,c-1) = iter+2; // make any of its dark neighbors bright.
          if (!im(r-1,c  )) im(r-1,c  ) = iter+2; // first iteration, we set new pixels to 2. Second iteration, they're set to 3, etc.
          if (!im(r-1,c+1)) im(r-1,c+1) = iter+2; 
          if (!im(r,  c-1)) im(r,  c-1) = iter+2;
          if (!im(r,  c+1)) im(r,  c+1) = iter+2;
          if (!im(r+1,c-1)) im(r+1,c-1) = iter+2;
          if (!im(r+1,c  )) im(r+1,c  ) = iter+2;
          if (!im(r+1,c+1)) im(r+1,c+1) = iter+2;
        } 
      } 
    }
  }
  // For any pixel we set to a positive value, set it to 1. 
  for (int r=0; r < im.nrow(); r++)
    for (int c=0; c < im.ncol(); c++)
      if (im(r,c) > 1)
        im(r,c)=1;

  return im;  
}



// [[Rcpp::export]]
IntegerMatrix boxErode(IntegerMatrix im, IntegerVector iters) {
  
  // We'll only iterate from the first to the last bright pixel of each row, rather than over the entire row.
  // Identify the column range of interest for each row: 
  
  IntegerMatrix startEndCols(im.nrow(), 2);
  int r,c;
  for (r=0; r < im.nrow(); r++){ // for each row
    for (c=0; c < im.ncol(); c++){
      if (im(r,c)){
        startEndCols(r,0) = c;
        break;
      }
    }
    for (c=im.ncol()-1; c >= 0; c--){
      if (im(r,c)){
        startEndCols(r,1) = c;
        break;
      }
    }
  }

  for (int iter=0; iter < iters[0]; iter++){
    for (int r=0; r < im.nrow(); r++){
      for (int c=startEndCols(r,0); c <= startEndCols(r,1); c++){
        
        //First, check if it's the first iteration and we're at an edge. If so, erode it (only if it's already bright).
        if ((r==0 || c==0 || r==im.nrow()-1  || c==im.ncol()-1)){
          if(im(r,c)>0) 
            im(r,c) = -(iter+1);
          continue;
        }
        if (im(r,c)>0) { // it's bright, and therefore an erosion candidate. 
          // check if any of its neighbors are dark and weren't set dark this iteration. If any are, erode it. 
          if      (im(r-1,c-1) <= 0 && im(r-1,c-1) > -(iter+1) ) im(r,c) = -(iter+1); 
          else if (im(r-1,c  ) <= 0 && im(r-1,c  ) > -(iter+1) ) im(r,c) = -(iter+1);
          else if (im(r-1,c+1) <= 0 && im(r-1,c+1) > -(iter+1) ) im(r,c) = -(iter+1);
          else if (im(r,  c-1) <= 0 && im(r,  c-1) > -(iter+1) ) im(r,c) = -(iter+1);
          else if (im(r,  c+1) <= 0 && im(r,  c+1) > -(iter+1) ) im(r,c) = -(iter+1);
          else if (im(r+1,c-1) <= 0 && im(r+1,c-1) > -(iter+1) ) im(r,c) = -(iter+1);
          else if (im(r+1,c  ) <= 0 && im(r+1,c  ) > -(iter+1) ) im(r,c) = -(iter+1);
          else if (im(r+1,c+1) <= 0 && im(r+1,c+1) > -(iter+1) ) im(r,c) = -(iter+1);
        } 
      } 
    }
  }
  // set any pixels that were eroded (therefore less than zero) to be zero. 
  for (int r=0; r < im.nrow(); r++)
    for (int c=0; c < im.ncol(); c++)
      if (im(r,c) < 0)
        im(r,c)=0;
      
  return im;  
}




/*** R
mat = matrix(0, ncol=10, nrow=15)
mat[5,7] = 1

image(boxDilate(mat,1))
image(boxDilate(mat,5))

system.time({
  for(j in 1:10000) boxDilate(mat,5)
  })

dilated = boxDilate(mat, 3)
image(dilated)
image(boxErode(dilated,1));
image(boxErode(dilated,2));
image(boxErode(dilated,3));
image(boxErode(dilated,4));



*/

