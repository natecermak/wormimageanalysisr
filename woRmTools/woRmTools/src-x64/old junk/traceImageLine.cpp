#include <Rcpp.h>
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//


// [[Rcpp::export]]
IntegerMatrix traceImageLine(IntegerMatrix im, IntegerMatrix coords) {
  // THIS FUNCTION RETURNS IN R COORDINATES
  if ((coords(0,0) <= 0) || (coords(0,1) <= 0) || (coords(0,0) > im.nrow()-1) || (coords(0,1) > im.ncol()-1) )
    return IntegerMatrix(-1,-1);
  int r = coords(0,0)-1;
  int c = coords(0,1)-1;
  int dr=0, dc=0;
  int i=1;

  Rcpp::IntegerMatrix im2(Rcpp::clone(im));

  do {
    im2(r,c) = 0;
    dr = 0;
    dc=0;
    if      (im2(r-1,c-1)==1) { dr=-1; dc=-1; }
    else if (im2(r-1,c  )==1) { dr=-1; dc= 0; }
    else if (im2(r-1,c+1)==1) { dr=-1; dc= 1; }
    else if (im2(r  ,c-1)==1) { dr= 0; dc=-1; }
    else if (im2(r  ,c+1)==1) { dr= 0; dc= 1; }
    else if (im2(r+1,c-1)==1) { dr= 1; dc=-1; }
    else if (im2(r+1,c  )==1) { dr= 1; dc= 0; }
    else if (im2(r+1,c+1)==1) { dr= 1; dc= 1; }

    if (dr==0 && dc==0) break;
    r+=dr;
    c+=dc;
    coords(i, 0) = r+1;
    coords(i, 1) = c+1;
    i++;
  } while (true);

  return coords;
}



// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically
// run after the compilation.
//

/*** R

mat = matrix(0,10,15)
mat[3:9, 2] = 1
mat[2, 3:12] = 1
init = matrix(0, nrow=sum(mat), ncol=2)
init[1,] = c(2,12)

coords = traceImageLine(mat, init)
*/
