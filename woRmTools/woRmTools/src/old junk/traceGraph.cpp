#include <Rcpp.h>
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//

// [[Rcpp::export]]
IntegerMatrix getNodesFromImage(IntegerMatrix im, IntegerVector maxNodes) {
  if (maxNodes[0] <= 0)
    return IntegerMatrix();
  IntegerMatrix np(maxNodes[0], 3);

  int nNodes=0; //endpoints found
  int nn; // number of directions one could go

  for (int i=1; i < im.nrow()-1; i++){ // deal with the center of the image only.
    for (int j=1; j < im.ncol()-1; j++){
      if (im(i,j)){
        nn = (!im(i-1,j-1) && im(i-1,j  )) + (!im(i-1,j  ) && im(i-1,j+1)) +
             (!im(i-1,j+1) && im(i  ,j+1)) + (!im(i,  j+1) && im(i+1,j+1)) +
             (!im(i+1,j+1) && im(i+1,j  )) + (!im(i+1,j  ) && im(i+1,j-1)) +
             (!im(i+1,j-1) && im(i  ,j-1)) + (!im(i  ,j-1) && im(i-1,j-1));

        if (nn == 1 || nn > 2){
          np(nNodes,0)=i;
          np(nNodes,1)=j;
          np(nNodes,2)=nn;
          nNodes++;
          if (nNodes == maxNodes[0]) //if it's full, return immediately
            return np;
        }
      }
    }
  }
  if (nNodes==0) return IntegerMatrix();

  return np( Range(0, nNodes-1), Range(0,2));
}



double dist(IntegerVector pt1, IntegerVector pt2){
  return sqrt(pow(pt1[0] - pt2[0], 2) + pow(pt1[1] - pt2[1], 2) );
}



int isNode(IntegerVector pt, IntegerMatrix np){
  for (int i=0; i< np.nrow(); i++)
    if (pt[0] == np(i,0) && pt[1] == np(i,1))
      return i;
  return -1;
}



NumericVector getNextNodeOnPath(IntegerMatrix im, IntegerMatrix np, IntegerVector curPoint, IntegerVector lastPoint){

  double distToNextNode = dist(lastPoint, curPoint);
  bool foundNeighbor;
  int nOptions;
  IntegerVector testPoint(2);
  int dr[] = {0, -1, 1, 0, -1,  1, -1, 1}; // test side pixels first, then diagonals
  int dc[] = {-1, 0, 0, 1, -1, -1,  1, 1};

  while ( isNode(curPoint, np) == -1){ // we're not currently at a node!
    foundNeighbor = false;

    nOptions = 0;
    for (int i=0; i < 8; i++){
      testPoint[0] = curPoint[0]+dr[i];
      testPoint[1] = curPoint[1]+dc[i];
      nOptions += (im(testPoint[0], testPoint[1]) && dist(testPoint, curPoint) < dist(testPoint, lastPoint));
    }

    for (int i=0; i < 8 && !foundNeighbor; i++){
      testPoint[0] = curPoint[0]+dr[i];
      testPoint[1] = curPoint[1]+dc[i];
      //if the neighbor is 1) bright, 2) closer to the current point than the last point (which immediately rules out the last point)
      if (im(testPoint[0], testPoint[1]) && dist(testPoint, curPoint) < dist(testPoint, lastPoint)) {
        if (nOptions == 1 || (nOptions > 1 && isNode(testPoint,np)!=-1) ){
          lastPoint[0] = curPoint[0];
          lastPoint[1] = curPoint[1];
          curPoint[0] += dr[i];
          curPoint[1] += dc[i];
          distToNextNode += dist(lastPoint, curPoint);
          foundNeighbor = true;
          break;
        }
      }
    }

    if (!foundNeighbor)
      break;
  }

  NumericVector nextNode(2);
  nextNode[0] = isNode(curPoint, np);
  nextNode[1] = distToNextNode;
  return nextNode;
}


// [[Rcpp::export]]
NumericMatrix getEdgeList(IntegerMatrix im, IntegerMatrix np, IntegerVector maxEdges) {
  /*
   * Function assumes we will start at the first node point in np, and work our way through.
   * Will return an edge list.
   */
  if (np.nrow() < 2 || maxEdges[0] < 1)
    return NumericMatrix(-1,-1);

  NumericMatrix edgeList = NumericMatrix(maxEdges[0],3);
  int numEdges =0;

  IntegerVector newPoint(2);
  IntegerVector lastPoint(2);
  NumericVector nextNode;

  int dr[] = {0, -1, 1, 0, -1,  1, -1, 1}; // test side pixels first, then diagonals
  int dc[] = {-1, 0, 0, 1, -1, -1,  1, 1};
  int nPathsExplored = 0;

  for (int nodeIndex=0; nodeIndex < np.nrow(); nodeIndex++){            // for every node
    nPathsExplored = 0;
    for (int i=0; i<8 && nPathsExplored<np(nodeIndex,2); i++){        // check every direction (in order above) from there
      if (im(np(nodeIndex, 0)+dr[i], np(nodeIndex, 1)+dc[i]) == 1) {  // if a direction has a path
        lastPoint[0] = np(nodeIndex, 0);
        lastPoint[1] = np(nodeIndex, 1);
        newPoint[0]  = np(nodeIndex, 0) + dr[i];
        newPoint[1]  = np(nodeIndex, 1) + dc[i];
        nextNode = getNextNodeOnPath(im, np, newPoint, lastPoint); // returns both the node index of the next node, and the distance to get there.
        edgeList(numEdges, 0) = nodeIndex;      // from
        edgeList(numEdges, 1) = nextNode[0];    // to
        edgeList(numEdges, 2) = nextNode[1];    // distance
        nPathsExplored++;
        numEdges++;
      }
    }
  }

  if (numEdges==0) return NumericMatrix();
  return edgeList( Range(0, numEdges-1), Range(0,2));
}



// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically
// run after the compilation.
//

/*** R

mat = matrix(0,10,10)
mat[2, 7] = 1
mat[3,4:6] = 1
mat[2:4,4] = 1
mat[5:7,3] = 1
mat[8:9,2] = 1
mat=t(mat)

np = getNodesFromImage(mat, 10);
np = np[order(np[,3]), ]
image(1:nrow(mat), 1:ncol(mat), mat)
points(np[,1:2]+1, pch=as.character(1:nrow(np)))


edgeList = getEdgeList(mat, np, 100)
for (j in 1:nrow(edgeList))
  lines(rbind(np[edgeList[j,1]+1, 1:2], np[edgeList[j,2]+1, 1:2]) + 1)


#coords = traceImageLine(mat, init)
*/
