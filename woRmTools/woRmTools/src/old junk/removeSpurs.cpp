#include <Rcpp.h>
using namespace Rcpp;



bool isEndPoint(IntegerMatrix im, int i, int j){
  if (im(i,j)==0) return false;
  int nn = im(i-1,j-1) + im(i,j-1) + im(i+1,j-1) + im(i-1,j+0) +  im(i+1,j+0) + im(i-1,j+1) + im(i,j+1) + im(i+1,j+1);
  if (nn == 1) return true;

  if (nn == 2 &&  ((im(i-1,j-1)&&im(i,j-1)) | (im(i,j-1)&&im(i+1,j-1)) | (im(i-1,j-1)&&im(i-1,j+0)) | (im(i-1,j+0)&&im(i-1,j+1)) |
      (im(i-1,j+1)&&im(i,j+1)) | (im(i,j+1)&&im(i+1,j+1)) | (im(i+1,j-1)&&im(i+1,j+0)) | (im(i+1,j+0)&&im(i+1,j+1)))  )
    return true;
  return false;
}


bool isSpurTip(IntegerMatrix im, int i, int j){
  if (im(i,j)==0) return false;
  int nn = im(i-1,j-1) + im(i,j-1) + im(i+1,j-1) + im(i-1,j+0) +  im(i+1,j+0) + im(i-1,j+1) + im(i,j+1) + im(i+1,j+1);
  if (nn == 1) return true;

  if (nn == 2 &&  ((im(i-1,j-1)&&im(i,j-1)) | (im(i,j-1)&&im(i+1,j-1)) | (im(i-1,j-1)&&im(i-1,j+0)) | (im(i-1,j+0)&&im(i-1,j+1)) |
      (im(i-1,j+1)&&im(i,j+1)) | (im(i,j+1)&&im(i+1,j+1)) | (im(i+1,j-1)&&im(i+1,j+0)) | (im(i+1,j+0)&&im(i+1,j+1)))  )
    return true;
  if (nn == 3 &&  ((im(i-1,j-1)&&im(i,j-1)&&im(i+1,j-1)) | (im(i-1,j+1)&&im(i,j+1)&&im(i+1,j+1)) |
                   (im(i-1,j-1)&&im(i-1,j)&&im(i-1,j+1)) | (im(i+1,j-1)&&im(i+1,j)&&im(i+1,j+1)) ) )
    return true;
  return false;
}



// [[Rcpp::export]]
IntegerMatrix endPointVector(IntegerMatrix im, IntegerVector maxEndPoints) {
  if (maxEndPoints[0] <= 0)
    return IntegerMatrix();
  IntegerMatrix ep(maxEndPoints[0], 2);

  int nep=0; //endpoints found
  for (int i=1; i < im.nrow()-1; i++){ // deal with the center of the image only.
    for (int j=1; j < im.ncol()-1; j++){
      if (im(i,j)){
        if (isEndPoint(im,i,j)){
          ep(nep,0)=i;
          ep(nep,1)=j;
          nep++;
          if (nep == maxEndPoints[0]) //if it's full, return immediately
            return ep;
        }
      }
    }
  }
  if (nep==0) return IntegerMatrix();

  return ep( Range(0, nep-1), Range(0,1));
}




// [[Rcpp::export]]
IntegerMatrix removeSpurs(IntegerMatrix im, IntegerVector maxIters) {

  IntegerMatrix epv = endPointVector(im, IntegerVector::create(100)); // this could fail if there are too many endpoints
  int nep = epv.nrow();
  if (nep <= 2)
    return im;

  int r, c, iters=0;

  Rcpp::IntegerMatrix im2(Rcpp::clone(im)); // create deep copy of image

  LogicalVector stillEndPoint(epv.nrow());
  for (int i = 0; i < stillEndPoint.size(); i++)
    stillEndPoint[i] = true;

  // this loop erodes from the endpoints (quickly).
  while (nep > 2 && iters < maxIters[0]){

    for (int i=0; i < epv.nrow(); i++){
      if (stillEndPoint[i]){
        r = epv(i,0);
        c = epv(i,1);
        im2(r,c) = 0;  // remove the point

        bool foundNewEndpoint = false;
        if      (isSpurTip(im2, r-1,c-1)) { epv(i,0) += -1; epv(i,1) += -1; foundNewEndpoint=true;}
        else if (isSpurTip(im2, r  ,c-1)) { epv(i,0) +=  0; epv(i,1) += -1; foundNewEndpoint=true;}
        else if (isSpurTip(im2, r+1,c-1)) { epv(i,0) +=  1; epv(i,1) += -1; foundNewEndpoint=true;}
        else if (isSpurTip(im2, r-1,c  )) { epv(i,0) += -1; epv(i,1) += 0;  foundNewEndpoint=true;}
        else if (isSpurTip(im2, r+1,c  )) { epv(i,0) +=  1; epv(i,1) += 0;  foundNewEndpoint=true;}
        else if (isSpurTip(im2, r-1,c+1)) { epv(i,0) += -1; epv(i,1) += 1;  foundNewEndpoint=true;}
        else if (isSpurTip(im2, r  ,c+1)) { epv(i,0) +=  0; epv(i,1) += 1;  foundNewEndpoint=true;}
        else if (isSpurTip(im2, r+1,c+1)) { epv(i,0) +=  1; epv(i,1) += 1;  foundNewEndpoint=true;}

        if (!foundNewEndpoint){
          stillEndPoint[i]= false;
          nep--;
          continue;
        }
        if (foundNewEndpoint){
          //verify that our new endpoint hasn't merged with another active endpoint
          for (int ii=0; ii<epv.nrow(); ii++){
            if (ii != i  && stillEndPoint[ii] && (epv(i,0) == epv(ii,0) && epv(i,1) == epv(ii,1))){
              stillEndPoint[i]= false;
              nep--;
              break;
            }
          }
        }
      }
    }
    iters++;
  } // end of eroding while loop


  int dr, dc, nn;
  for (int i=0; i < epv.nrow(); i++){
    if (stillEndPoint[i]) {// this is one of the endpoints we need to rebuild from.

      r = epv(i,0);
      c = epv(i,1);
      // figure out how many neighbors in the original image.
      nn = im(r-1,c-1) + im(r,c-1) + im(r+1,c-1) + im(r-1,c) + im(r+1,c) + im(r-1,c+1) +im(r,c+1) + im(r+1,c+1);
      while (nn==2){
        dr = 0;
        dc = 0;
        if (im(r-1,c-1) && !im2(r-1,c-1)) { dr=-1; dc=-1; }
        if (im(r-1,c  ) && !im2(r-1,c  )) { dr=-1; dc= 0; }
        if (im(r-1,c+1) && !im2(r-1,c+1)) { dr=-1; dc= 1; }
        if (im(r  ,c-1) && !im2(r,  c-1)) { dr= 0; dc=-1; }
        if (im(r  ,c+1) && !im2(r,  c+1)) { dr= 0; dc= 1; }
        if (im(r+1,c-1) && !im2(r+1,c-1)) { dr= 1; dc=-1; }
        if (im(r+1,c  ) && !im2(r+1,c  )) { dr= 1; dc= 0; }
        if (im(r+1,c+1) && !im2(r+1,c+1)) { dr= 1; dc= 1; }

        if (dr == 0 && dc == 0) break;
        else {
          r += dr;
          c += dc;
          im2(r,c) = 1;
          nn = im(r-1,c-1) + im(r,c-1) + im(r+1,c-1) + im(r-1,c) + im(r+1,c) + im(r-1,c+1) +im(r,c+1) + im(r+1,c+1);
        }
      }
    }
  }

  return im2;
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically
// run after the compilation.
//

/*** R
spurMatrix = matrix(0, ncol=10,nrow=15)
spurMatrix[2:9, 2] = 1
spurMatrix[2, 2:9] = 1
spurMatrix[7, 2:8] = 1
spurMatrix = thinImage(spurMatrix,100);
image(spurMatrix)

spursRemoved = removeSpurs(spurMatrix , 100);
image(spursRemoved)

*/
