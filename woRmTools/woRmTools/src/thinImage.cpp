#include <Rcpp.h>
using namespace Rcpp;


// [[Rcpp::export]]
IntegerMatrix thinImage(IntegerMatrix im, NumericVector maxIters) {

  int LUT1[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,0,0,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
  int LUT2[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,0,0,1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,0,0,1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,0,0,1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,0,0,1,1,0,1,1,1};
  int lutVal;
  int r,c;
  int iters = 0;
  bool changedSomething=true;

  //Set the edges of the image to zero;
  for (int r=0; r<im.nrow(); r++){ 
    im(r,0) = 0; // set the first column to zero
    im(r,im.ncol()-1) = 0; // set the last column to zero.
  }
  for (int c=0; c<im.ncol(); c++){ //set the first and last rows to zero.
    im(0,c) = 0;
    im(im.nrow()-1,c) = 0;
  }
  
  
  // Rather than looping over the entire n x m matrix, for each row, we'll loop only from the first column with 
  // a bright pixel to the last column with a bright pixel. So before the first iteration, we identify these columns.
  IntegerMatrix startEndCols(im.nrow(), 2);
  for (r=1; r < im.nrow()-1; r++){ 
    for (c=1; c < im.ncol()-1; c++){ // find the first bright pixel in the row
      if (im(r,c)==1){ 
        startEndCols(r,0) = c;
        break;
      }
    }
    for (c=im.ncol()-2; c > 0; c--){ // find the last bright pixel in the row
      if (im(r,c)==1){
        startEndCols(r,1) = c;
        break;
      }
    }
  }

  // Now we'll iteratively apply the two lookup tables. 
  // Rather than directly setting pixels to zero, we'll set them to -(iter+1) [-1, -2, -3, -4 ...]
  // and we'll treat any negative number as 0 unless we just set it to zero this iteration.
  // At the end, we'll set all negative numbers to zero. 
  while(changedSomething && iters < 2*maxIters[0]){
    iters++;
    changedSomething = false;

    for (r=1; r < im.nrow()-1; r++){ // skip the first and last rows
      for (c=startEndCols(r,0); c <= startEndCols(r,1); c++){ // from the first bright pixel to the last bright pixel
        if (im(r,c)==1){ // it's bright - we might want to erode it.
          lutVal =   (im(r-1,c-1)==1 || im(r-1,c-1)==-(iters+1)) +  // check if neighboring pixel is bright or was eroded THIS iteration.
                   2*(im(r  ,c-1)==1 || im(r  ,c-1)==-(iters+1)) +   
                   4*(im(r+1,c-1)==1 || im(r+1,c-1)==-(iters+1)) +
                   8*(im(r-1,c  )==1 || im(r-1,c  )==-(iters+1)) +  
                  16            +
                  32*(im(r+1,c  )==1 || im(r+1,c  )==-(iters+1)) +
                  64*(im(r-1,c+1)==1 || im(r-1,c+1)==-(iters+1)) + 
                 128*(im(r  ,c+1)==1 || im(r  ,c+1)==-(iters+1)) + 
                 256*(im(r+1,c+1)==1 || im(r+1,c+1)==-(iters+1));
          if (!LUT1[lutVal]){ // then we need to erode this point! 
            im(r,c) = -(iters+1);
            changedSomething=true;
          }
        }
      }
    }
    iters++;
    // now redo the same thing as above, but with the other lookup table. 
    for (r=1; r < im.nrow()-1; r++){ // skip the first and last rows
      for (c=startEndCols(r,0); c <= startEndCols(r,1); c++){ // from the first bright pixel to the last bright pixel
        if (im(r,c)==1){ // it's bright - we might want to erode it.
          lutVal =   (im(r-1,c-1)==1 || im(r-1,c-1)==-(iters+1)) +  // check if neighboring pixel is bright or was eroded THIS iteration.
                   2*(im(r  ,c-1)==1 || im(r  ,c-1)==-(iters+1)) +   
                   4*(im(r+1,c-1)==1 || im(r+1,c-1)==-(iters+1)) +
                   8*(im(r-1,c  )==1 || im(r-1,c  )==-(iters+1)) +  
                  16            +
                  32*(im(r+1,c  )==1 || im(r+1,c  )==-(iters+1)) +
                  64*(im(r-1,c+1)==1 || im(r-1,c+1)==-(iters+1)) + 
                 128*(im(r  ,c+1)==1 || im(r  ,c+1)==-(iters+1)) + 
                 256*(im(r+1,c+1)==1 || im(r+1,c+1)==-(iters+1));
          if (!LUT2[lutVal]){
            im(r,c) = -(iters+1);
            changedSomething=true;
          }
        }
      }
    }
  }
  // now set each negative pixel to zero, then return the image.
  for (r=0; r < im.nrow(); r++)
    for (c=0; c < im.ncol(); c++)
      if (im(r,c) < 0)
        im(r,c) = 0;
      
  return im;
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//

/*** R

needsThinning = matrix(0,ncol=10,nrow=10)
needsThinning[2:4, 2:9] = 1
needsThinning[2:9, 2:4] = 1

a = thinImage(im = needsThinning, maxIters=100)
image(a)
*/
